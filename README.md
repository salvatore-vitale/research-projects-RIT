# research-projects-RIT
Clean version of research-projects, just with ILE

NOTICE:  For historical reasons, the principal branch is *not* the master branch.  We will update this notice periodically with relevant informatoin.
  * At present (20190310) analysis and development are performed with ``temp-RIT-Tides-port_master-GPUIntegration``.  Current tutorials and instructions are available with this branch.
  * For the analysis compatible with ``lalinference_o2`` branch, please use ``temp-RIT-Tides_PreserveOld``
